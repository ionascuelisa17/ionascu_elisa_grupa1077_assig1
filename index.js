
const FIRST_NAME = "Elisa";
const LAST_NAME = "Ionascu";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(input, property) {
    if (typeof (input) == "string")
        return parseInt(input);
    else if (input == Number.NEGATIVE_INFINITY || input == Number.POSITIVE_INFINITY)
        return NaN;
    else if (input > Number.MAX_SAFE_INTEGER || input < Number.MIN_SAFE_INTEGER)
        return NaN;
    else if (isNaN(input) == true) return NaN;
    else return parseInt(input);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

